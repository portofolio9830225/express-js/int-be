const env = require("dotenv").config();
const request = require("supertest");
const app = require("../../server");
const api = request(app);
const jwt = require("jsonwebtoken");

const appToken = jwt.sign(
  {
    name: process.env.NAME,
    type: "private_apps",
  },
  process.env.PRIVATE_KEY,
  { expiresIn: String(process.env.SESSION_TIME) }
);
const userResultAfterInsert = {};
const userToken = jwt.sign(
  {
    username: "TEST",
    password: "TEST",
    details: userResultAfterInsert,
  },
  process.env.PRIVATE_KEY,
  { expiresIn: String(process.env.SESSION_TIME) }
);

const xUserToken = `Bearer ${userToken}`;
const xAppToken = `Bearer ${appToken}`;
const reqWithAuth = async (route, method, body = []) => {
  if (method == "get") {
    return api
      .get(route)
      .set("Content-type", "application/json")
      .set("X-Application-Token", xAppToken)
      .set("X-User-Token", xUserToken);
  }
  if (method == "post") {
    return api
      .post(route)
      .send(body)
      .set("Content-type", "application/json")
      .set("X-Application-Token", xAppToken)
      .set("X-User-Token", xUserToken);
  }
  if (method == "patch") {
    return api
      .patch(route)
      .send(body)
      .set("Content-type", "application/json")
      .set("X-Application-Token", xAppToken)
      .set("X-User-Token", xUserToken);
  }
  if (method == "put") {
    return api
      .put(route)
      .send(body)
      .set("Content-type", "application/json")
      .set("X-Application-Token", xAppToken)
      .set("X-User-Token", xUserToken);
  }
  if (method == "delete") {
    return api
      .delete(route)
      .send(body)
      .set("Content-type", "application/json")
      .set("X-Application-Token", xAppToken)
      .set("X-User-Token", xUserToken);
  }
  return;
};

module.exports = { reqWithAuth };
