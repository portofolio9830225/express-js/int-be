const schemaDataCustomerValidator = {
  type: "object",
  properties: {
    _id: { type: "string" },
    requester: { type: "string" },
    department: { type: "string" },

    customer_name: { type: "string" },
    country: { type: "string" },
    contact_person: { type: "array" },
    contact_detail: { type: "array" },
    sales_person: { type: "string" },
    details: { type: "array" },
    qty_year_1: { type: "number" },
    qty_year_2: { type: "number" },
    comercial_lost: { type: "number" },
    top: { type: "string" },

    created_at: { type: "string" },
    created_by: { type: "string" },
    updated_at: { type: "string" },
    updated_by: { type: "string" },
    deleted_at: { type: "string" },
    deleted_by: { type: "string" },
  },
};

const bodyDataCustomer = {
  requester: "LNK000784",
  department: "MIS",
  data: [
    {
      customer_name: "MISR CAFÉ",
      country: "EGYPT",
      top: "100% DAP",
      comercial_lost: 210000,
      sales_person: "LNK000632",
      contact_person: [
        { name: "Dr. Mounir", position: "CEO", email: "misrcafe@link.net" },
        { name: "Sanaa", position: "R&D", email: "sanaa@misrcafe.com.eg" },
      ],
      contact_detail: [
        "Phone. : +20222908374 - 24196118",
        "61 SAYED EL MARGHANY STREET",
        "HELIOPOLIS CAIRO - EGYPT",
      ],
      details: [
        {
          product_grade: "LK 35 CA",
          packing_size: 25,
          fob_price: 1.4,
          qty_year_1: 25,
          qty_year_2: 325,
          potential_per_month: 100,
          potential_per_year: 1200,
          share_year_1: 14,
          share_year_2: 350,
          remarks:
            "- Lost 6 Bulan (35CA - Jan-Jun'17) ; Particle Size (dusty) & Taste Problem",
        },
        {
          product_grade: "LK 07 HF",
          packing_size: 10,
          fob_price: 2.4,
          qty_year_1: 10,
          qty_year_2: 100,
          potential_per_month: 10,
          potential_per_year: 120,
          share_year_1: 40,
          share_year_2: 70,
          remarks:
            "- Lost 2 Bulan (LK07HF - Jan-Feb'17) ; Taste Development (Kievit)",
        },
      ],
    },
    {
      customer_name: "INTERGRUP",
      country: "RUSSIA",
      top: "100% TT in Adv.",
      sales_person: "LNK000632",
      contact_person: [
        {
          name: "Tatiana Smirnova",
          position: "Manager",
          email: "intergrup@yandex.ru",
        },
      ],
      contact_detail: ["Phone : +7 495 6452415"],
      details: [
        {
          product_grade: "LK 35 CA",
          packing_size: 25,
          fob_price: 1.69,
          qty_year_2: 5,
        },
        {
          product_grade: "LK 07 HF",
          packing_size: 10,
          fob_price: 2.55,
          qty_year_2: 10,
        },
      ],
    },
    {
      customer_name: "GOLD LEAF",
      country: "MALAYSIA",
      top: "100% DAP",
      comercial_lost: 186000,
      sales_person: "LNK000632",
      contact_person: [
        {
          name: "Kelly Hng",
          position: "Purchasing",
          email: "info@goldleaf.com.my",
        },
      ],
      contact_detail: [
        "NO. 8, LORONG JELAWAT 4, ",
        "SEBERANG JAYA INDUSTRIAL PARK, ",
        "BANDAR SEBERANG JAYA, PRAI,",
        " PULAU PINANG 13700 MALAYSIA",
        "Phone : +604 351 2532/ 4044/ 4043",
      ],
      details: [
        {
          product_grade: "LK 35 BB",
          packing_size: 25,
          fob_price: 1.24,
          qty_year_2: 225,
          potential_per_month: 25,
          potential_per_year: 300,
          share_year_1: 25,
          share_year_2: 150,
          remarks:
            "- Lost 6 bulan (Januari-Juni), karena harga dan ganti grade (32CC-->35BB)",
        },
      ],
    },
    {
      customer_name: "HONSEI",
      country: "SINGAPORE",
      top: "100% DAP",
      sales_person: "LNK000632",
      contact_person: [
        {
          name: "Sungling",
          position: "Purchasing",
          email: "sungling@honseigroup.com",
        },
      ],
      contact_detail: [
        "71 Toh Guan Road East ",
        "#06-07 TCH Tech Centre Singapore 608598 ",
        "Phone : +65 6842 4600",
      ],
      details: [
        {
          product_grade: "LK 32 CC",
          packing_size: 25,
          fob_price: 1.35,
          qty_year_2: 98,
          potential_per_month: 7,
          potential_per_year: 84,
          share_year_1: 28,
          share_year_2: 42,
          remarks: "- 14 mt per 2 month",
        },
      ],
    },
    {
      customer_name: "RAJASA DE CORO",
      country: "COSTA RICA",
      top: "100% TT in Adv.",
      sales_person: "LNK000632",
      contact_person: [
        {
          name: "Joachim Richly",
          position: "Owner",
          email: "joachimrichly@yahoo.com",
        },
      ],
      contact_detail: [
        "Santa Rosa 700 mtrs Este de Auto Xiri. ",
        "Condominio Bodegas El Sol, local # 32 Heredia,",
        " Costa Rica",
        "Phone : (506) 2262-6523 ",
        "(506) 2260-5001 (506) 2262-6507",
        "Mobile : (506) 8332-5571 ",
      ],
      details: [
        {
          product_grade: "LK 32 BA",
          packing_size: 25,
          fob_price: 1.1,
          qty_year_2: 13,
          potential_per_month: 2,
          potential_per_year: 28,
          remarks: "- Sailing Time : 60 hari",
        },
      ],
    },
  ],
};

module.exports = {
  schemaDataCustomerValidator,
  bodyDataCustomer,
};
