const env = require("dotenv").config();
process.env.NODE_ENV = "test";
const {
  schemaDataCustomerValidator,
  bodyDataCustomer,
} = require("./schemaValidator/dataCustomer_schemaValidator");
const debug = require("debug")("backend-int:customer-test");
const db = require("../models");
const { reqWithAuth } = require("./helper/auth");
//jest json schema
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.customer.deleteMany({});
});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  // db.mongoose.connection.close();
  db.mongoose.close();
  done();
});

describe("CREATE READ INT", () => {
  it("CRUD /api/customer/", async () => {
    // const { requester, department } = bodyDataCustomer;
    // //create
    // const createDataCustomer = await reqWithAuth(
    //   "/api/customer/bulk",
    //   "post",
    //   bodyDataCustomer
    // );
    // expect(createDataCustomer.status).toEqual(200);
    // expect(createDataCustomer.body).toMatchSchema(schemaDataCustomerValidator);
    // // read
    // const readDataCustomer = await reqWithAuth(
    //   `/api/customer?department=${department}&requester=${requester}`,
    //   "get"
    // );
    // expect(readDataCustomer.status).toEqual(200);
    // expect(readDataCustomer.body).toMatchSchema(schemaDataCustomerValidator);
  });
});

describe("CREATE READ INT UNEXPECTED", () => {
  it("CRUD /api/customer/", async () => {
    // const requester = "";
    // const department = "";
    // //create
    // const createDataCustomer = await reqWithAuth(
    //   "/api/customer/bulk",
    //   "post",
    //   {}
    // );
    // expect(createDataCustomer.status).toEqual(400);
    // expect(createDataCustomer.body).toMatchSchema(schemaDataCustomerValidator);
    // // read
    // const readDataCustomer = await reqWithAuth(
    //   `/api/customer?department=${department}&requester=${requester}`,
    //   "get"
    // );
    // expect(readDataCustomer.status).toEqual(200);
    // expect(readDataCustomer.body).toMatchSchema(schemaDataCustomerValidator);
  });
});
