const _ = require("lodash");
const moment = require("moment");
const jwt_decode = require("jwt-decode");

function isEmptyObject(obj) {
  return obj == null || !Object.keys(obj).length;
}

function isEmptyArray(arr) {
  return arr == null || !arr.length;
}

function hasKey(obj, key) {
  return _.has(obj, key) && obj[key] != null;
}

function getCode(text = "", replace = "_") {
  return text.replace(/[^a-zA-Z0-9]/g, replace).toLowerCase();
}

function getCurrrentUser(req) {
  const userToken = req.header("x-user-token");
  const decodeToken = jwt_decode(userToken);
  return decodeToken.details.hris_org_tree.current_person;
}

function getFormatDate(serial, format = "YYYY-MM-DD") {
  const utc_days = Math.floor(serial - 25569);
  const utc_value = utc_days * 86400;
  const date_info = new Date(utc_value * 1000);
  const date_value = moment(date_info).format(format);
  return date_value;
}

function getCurrrentUser(req) {
  const userToken = req.header('x-user-token')
  const decodeToken = jwt_decode(userToken);
  return decodeToken.details.hris_org_tree.current_person
}

module.exports = {
  isEmptyObject,
  isEmptyArray,
  hasKey,
  getCode,
  getCurrrentUser,
  getFormatDate,
};
