const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:authorization-routes");
const authorizationModule = require("../modules/authorizationModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);
    let result = {};
    result = await authorizationModule.readAllAuthorization(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/:role", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);
    let result = {};
    const { role } = req.params;
    if (!helper.isEmptyObject(role)) {
      let payl = { role: role };
      result = await authorizationModule.readByRoleAuthorization(payl);
    }
    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
router.post(
  "/",
  [BackendValidator.isValidRequest, BackendValidator.checkDuplicateRole],
  async (req, res) => {
    try {
      let payload = req.body;
      const result = await authorizationModule.createAuthorization(payload);
      debug(result);
      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await authorizationModule.updateAuthorization(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await authorizationModule.deleteAuthorization(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
