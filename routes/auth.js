const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:auth-routes");
const authModule = require("../modules/authModule");
const BackendValidator = require("../middlewares/BackendValidator");

router.post("/signin", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await authModule.login(payload);
    debug(result);
    if (!result) return res.status(500).send("Server Error! Contact Admin!");
    if (result.error) return res.status(400).send(result);
    return res.status(result.status).send(result.data);
  } catch (err) {
    debug(err.response);
  }
});

router.post("/app-token", BackendValidator.isValidApp, async (req, res) => {
  try {
    const appName = req.header("x-app-name");
    const publicKey = req.header("x-public-key");
    const payload = {
      name: appName,
      publicKey: publicKey,
    };
    const result = await authModule.createAppToken(payload);
    debug(result);
    if (result.error) return res.status(400).send(result);
    return res.status(200).send(result);
  } catch (err) {
    debug(err.response);
    return res.status(400).send(err.response);
  }
});

router.post("/signup", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await authModule.register(payload);
    debug(result);
    if (!result) return res.status(500).send("Server Error! Contact Admin!");
    if (result.error) return res.status(400).send(result);
    return res.status(200).send(result);
  } catch (err) {
    debug(err.response);
  }
});
module.exports = router;
