const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:menu-routes");
const menuModule = require("../modules/menuModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);
    let result = {};
    result = await menuModule.readAllMenu(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/:id", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);
    let result = {};
    const { id } = req.params;
    if (!helper.isEmptyObject(id)) {
      if (id === "all") {
        result = await menuModule.readAllMenuSubmenu();
      } else {
        let payl = { _id: id };
        result = await menuModule.readByIdMenu(payl);
      }
    }
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post(
  "/",
  [BackendValidator.isValidRequest, BackendValidator.checkDuplicateNameLink],
  async (req, res) => {
    try {
      const payload = req.body;
      debug(payload);

      const result = await menuModule.createMenu(payload);
      debug(result);
      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await menuModule.updateMenu(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await menuModule.deleteMenu(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
