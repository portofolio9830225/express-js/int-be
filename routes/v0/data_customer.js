const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:data-customer-routes");
const DataCustomerModule = require("../../modules/v0/dataCustomerModule");
const BackendValidator = require("../../middlewares/BackendValidator");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await DataCustomerModule.create(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/bulk", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await DataCustomerModule.createBulk(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;

    payload.access_token = req.headers["x-user-token"].split(" ")[1];

    const result = await DataCustomerModule.read(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/bulk", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;

    payload.access_token = req.headers["x-user-token"].split(" ")[1];

    const result = await DataCustomerModule.readBulk(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
