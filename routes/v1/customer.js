const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:customer-routes");
const customerModule = require("../../modules/v1/customerModule");
const BackendValidator = require("../../middlewares/BackendValidator");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await customerModule.create(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/bulk", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await customerModule.createBulk(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    payload.showAll = ["true", "1"].includes(payload?.showAll?.toLowerCase());
    payload.user_token = req.header("x-user-token");

    const result = await customerModule.read(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get(
  "/generate-excel",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.query;
      payload.user_token = req.header("x-user-token");

      const result = await customerModule.generateExcel(payload);

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await customerModule.update(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await customerModule.destroy(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
