const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:transaksi-routes");
const BackendValidator = require("../../../middlewares/BackendValidator");
const helper = require("../../../common/helper");
const multer = require("multer");
const DIR = "./public/tmp-upload";

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null, `${file.fieldname}${Date.now()}${file.originalname}`);
    },
  });
  
const upload = multer({ storage, limits: { fieldSize: 25 * 1024 * 1024 }}).array("files");

const importTransactionModule = require("../../../modules/v2/rio/importTransactionModule");

router.post("/upload", BackendValidator.isValidRequest, async (req, res) => {
  const currentUser = helper.getCurrrentUser(req);

  try{
    const x_app_token_ = req.header("x-application-token");
    upload(req, res, async (err) => {
      if (err) {
        debug(err);
        return res.status(400).send("Upload Error! Contact Admin!");
      }
      const payload = {
          ...req.body,
          files: req.files,
          app_token: x_app_token_
      }
      const result = await importTransactionModule.upload(payload, currentUser);
      return res.send(result);
    });
    } catch(error) {
      debug(error);
      return res.status(500).send(err);
    }
});

router.get("/file-history", BackendValidator.isValidRequest, async(req, res) => {
  try {
    let result = {};
    result = await importTransactionModule.readHistory();
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (error) {
    debug(error);
    return res.status(500).send(error);
  }
});

router.get("/download", async (req, res) => {
  try {
    const x_app_token_ = req.header("x-application-token");
    const payload = {
      id: req.query,
      app_token: x_app_token_,
    };
    let result = {};
    result = await importTransactionModule.downloadFile(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;