const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:customer-routes");
const BackendValidator = require("../../middlewares/BackendValidator");

const customerModule = require("../../modules/v2/customerModule");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await customerModule.create(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.post("/bulk", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await customerModule.createBulk(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.user_token = req.header("x-user-token");
		payload.showAll = ["true", "1"].includes(payload?.showAll?.toLowerCase());
		payload.underLevel = ["true", "1"].includes(payload?.underLevel?.toLowerCase());

		const result = await customerModule.read(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/all", async (req, res) => {
	try {
		const result = await customerModule.readAll();

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await customerModule.update(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await customerModule.destroy(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.post("/store", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await customerModule.store(payload);

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/readTable", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;

		const result = await customerModule.readTable(payload);

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.delete("/deleteById", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await customerModule.deleteById(payload);

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.put("/updateById", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await customerModule.updateById(payload);

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

module.exports = router;
