const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:transaksi-routes");
const BackendValidator = require("../../middlewares/BackendValidator");

const transaksiModule = require("../../modules/v2/transaksiModule");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await transaksiModule.create(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.showAll = ["true", "1"].includes(payload?.showAll?.toLowerCase());

		const result = await transaksiModule.read(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/adjust", async (req, res) => {
	try {
		const payload = req.query;
		debug(payload);
		const result = await transaksiModule.readAdjust(payload);

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.post("/adjust", async (req, res) => {
	try {
		const payload = req.body;
		const result = await transaksiModule.createAdjust(payload.data);

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await transaksiModule.update(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await transaksiModule.destroy(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

module.exports = router;
