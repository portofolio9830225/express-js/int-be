const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:product-routes");
const BackendValidator = require("../../../middlewares/BackendValidator");

const productModule = require("../../../modules/v2/azhar/productModule");

router.get("/all", async (req, res) => {
	try {
		const result = await productModule.readAll();

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

module.exports = router;
