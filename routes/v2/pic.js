const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-int:data:pic-routes");
const BackendValidator = require("../../middlewares/BackendValidator");

const picModule = require("../../modules/v2/picModule");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await picModule.create(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    payload.showAll = ["true", "1"].includes(payload?.showAll?.toLowerCase());

    const result = await picModule.read(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await picModule.update(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await picModule.destroy(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
