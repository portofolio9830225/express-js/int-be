const mongoose = require("mongoose");
const database = require("../config/database");

const Authorization = database.model(
  "Authorization",
  new mongoose.Schema({
    role: { type: String, required: true },
    menu: { type: Object, required: true },
    isDeleted: { type: Boolean },
  }),
  "authorizations"
);
module.exports = Authorization;
