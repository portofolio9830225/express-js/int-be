const mongoose = require("mongoose");
const database = require("../config/database");

const User = database.model(
  "User",
  new mongoose.Schema({
    name: { type: String, required: true },
    public_key: { type: String, required: true },
    private_key: { type: String, required: true },
    type: { type: String },
    isValidated: { type: Boolean },
    isDeleted: { type: Boolean },
    details: Array,
    auth_type: { type: String },
  }),
  "users"
);
module.exports = User;
