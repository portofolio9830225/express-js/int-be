// const database = require("../config/database");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db_index = require("./v2");

const db = { ...db_index };
// db.mongoose = database;
db.mongoose = mongoose;

// init menu
db.authorization = require("./authorization.model");
db.authentication = require("./authentication.model");
db.menu = require("./menu.model");
db.set_active_log = require("./set_active_log.model");
db.submenu = require("./submenu.model");
db.user = require("./user.model");
db.userreg = require("./userreg.model");
db.history = require("./history.model");

module.exports = db;
