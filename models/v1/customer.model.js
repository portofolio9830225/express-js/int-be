const mongoose = require("mongoose");
const database = require("../../config/database");

const Customer = database.model(
  "Customer V1",
  new mongoose.Schema({
    is_active: { type: Boolean, required: true },
    department: { type: String, required: true },
    customer_name: { type: String, required: true },
    country: { type: String, required: true },
    version: { type: String },
    contact_person: { type: Array, required: true },
    contact_detail: { type: Array, required: true },
    sales_person: { type: String },
    details: { type: Array, required: true },
    qty_year_1: { type: Number },
    qty_year_2: { type: Number },
    comercial_lost: { type: Number },
    top: { type: String },
    created_at: Date,
    created_by: { type: String },
    updated_at: Date,
    updated_by: { type: String },
    deleted_at: Date,
    deleted_by: { type: String },
  }),
  "data_customer"
);
module.exports = Customer;
