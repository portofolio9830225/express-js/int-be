const database = require("../../config/database");
database.Promise = global.Promise;

const db = {};
db.mongoose = database;

// add here
db.customer = require("./customer.model");

module.exports = db;
