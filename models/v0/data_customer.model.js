const mongoose = require("mongoose");
const database = require("../../config/database");

const DataCustomer = database.model(
  "DataCustomer V0",
  new mongoose.Schema({
    is_active: { type: Boolean, required: true },
    department: { type: String, required: true },
    customer_name: { type: String, required: true },
    country: { type: String, required: true },
    version: { type: String, required: true },
    version_ots: { type: String },
    contact_person: { type: Array, required: true },
    contact_detail: { type: Array, required: true },
    sales_person: { type: String, required: true },
    details: { type: Array, required: true },
    qty_year_1: { type: Number },
    qty_year_2: { type: Number },
    comercial_lost: { type: Number },
    top: { type: String },
    created_at: Date,
    created_by: { type: String },
    updated_at: Date,
    updated_by: { type: String },
    deleted_at: Date,
    deleted_by: { type: String },
  }),
  "data_customer"
);
module.exports = DataCustomer;
