const database = require("../../config/database");
database.Promise = global.Promise;

const db = {};
db.mongoose = database;

// add here
db.data_customer = require("./data_customer.model");
db.one_time_script = require("./one_time_script.model");

module.exports = db;
