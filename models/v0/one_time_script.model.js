const mongoose = require("mongoose");
const database = require("../../config/database");

const OneTimeScript = database.model(
  "OneTimeScript V0",
  new mongoose.Schema({
    is_active: { type: Boolean, required: true },
    department: { type: String, required: true },
    version: { type: String, required: true },
    created_at: Date,
    created_by: { type: String },
    updated_at: Date,
    updated_by: { type: String },
    deleted_at: Date,
    deleted_by: { type: String },
  }),
  "one_time_script"
);
module.exports = OneTimeScript;
