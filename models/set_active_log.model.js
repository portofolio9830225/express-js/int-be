const mongoose = require("mongoose");
const database = require("../config/database");

const SetActiveLog = database.model(
  "SetActiveLog",
  new mongoose.Schema({
    is_active: { type: Boolean },
  }),
  "set_active_log"
);
module.exports = SetActiveLog;
