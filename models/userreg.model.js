const mongoose = require("mongoose");
const database = require("../config/database");

const UserReg = database.model(
  "UserReg",
  new mongoose.Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String },
    type: { type: String },
    isValidated: { type: Boolean },
    isDeleted: { type: Boolean },
    details: Array,
    auth_type: { type: String },
  }),
  "users"
);
module.exports = UserReg;
