const mongoose = require("mongoose");
const database = require("../config/database");

const Authentication = database.model(
  "Authentication",
  new mongoose.Schema({
    username: { type: String, required: true },
    is_active: { type: Boolean },
  }),
  "authentications"
);
module.exports = Authentication;
