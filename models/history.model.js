const mongoose = require("mongoose");
const database = require("../config/database");

const History = database.model(
  "History",
  new mongoose.Schema({
    id_history: { type: mongoose.Schema.Types.ObjectId, required: true },
    collection_name: { type: String, required: true },
    data: { type: Object, required: true },
    created_at: { type: Date, required: true },
    created_by: { type: String, required: true },
  }),
  "history"
);
module.exports = History;
