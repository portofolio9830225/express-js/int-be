const mongoose = require("mongoose");
const database = require("../../../config/database");

const Transaction = database.model(
	"Transaction",
	new mongoose.Schema({
		year: { type: String, default: "" },
		periode: { type: String, default: "" },
		customer_code: { type: String, default: "" },
		customer_name: { type: String, default: "" },
		country: { type: String, default: "" },
		region: { type: String, default: "" },
		product_category_1: { type: String, default: "" },
		product_category_2: { type: String, default: "" },
		product_code: { type: String, default: "" },
		product_name: { type: String, default: "" },
		quantity: { type: String, default: "" },
		sales_value: { type: String, default: "" },
		cost_of_rev_trading: { type: Number, default: 0 },
		cost_of_rev_shortage: { type: Number, default: 0 },
		cost_of_rev_prod_var: { type: Number, default: 0 },
		cost_of_rev_dept_var: { type: Number, default: 0 },
		cost_of_rev_adj_oth: { type: Number, default: 0 },
		cost_of_variance_sales: { type: Number, default: 0 },
		cogs_value: { type: Number, default: 0 },
		gross_profit: { type: Number, default: 0 },
		price: { type: Number, default: 0 },
		cogs: { type: Number, default: 0 },
		gp_in: { type: Number, default: 0 },
		avg_qty: { type: Number, default: 0 },
		avg_sales_value: { type: Number, default: 0 },
		avg_cogs_value: { type: Number, default: 0 },
		variance: { type: Number, default: 0 },
	}),
	"transactions"
);
module.exports = Transaction;
