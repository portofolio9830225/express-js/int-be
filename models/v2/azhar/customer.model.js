const mongoose = require("mongoose");
const database = require("../../../config/database");

const MasterCustomer = database.model(
	"MasterCustomer",
	new mongoose.Schema({
		customer_code: { type: String, default: "" },
		customer_name: { type: String, default: "" },
		segment: { type: String, default: "" },
		"2_phase_country": { type: String, default: "" },
		"2_phase_region": { type: String, default: "" },
		"3_phase_country": { type: String, default: "" },
		"3_phase_region": { type: String, default: "" },
		top: { type: String, default: "" },
		is_active: { type: Boolean, default: true },
		requester: { type: String, default: "" },
	}),
	"master_customers"
);
module.exports = MasterCustomer;
