const mongoose = require("mongoose");
const database = require("../../../config/database");

const MasterProduct = database.model(
	"MasterProduct",
	new mongoose.Schema({
		product_mid: { type: String, default: "" },
		product_category: { type: String, default: "" },
		product_code: { type: String, default: "" },
		product_name: { type: String, default: "" },
	}),
	"master_product"
);
module.exports = MasterProduct;
