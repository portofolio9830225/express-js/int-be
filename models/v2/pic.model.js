const mongoose = require("mongoose");
const database = require("../../config/database");

const PIC = database.model(
  "PIC V2",
  new mongoose.Schema({
    id_customer: { type: mongoose.Schema.Types.ObjectId, required: true },
    name: { type: String, default: "" },
    position: { type: String, default: "" },
    phone: { type: mongoose.Schema.Types.Mixed, default: "" },
    email: { type: String, default: "" },
    remarks: { type: String, default: "" },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
    deleted_at: { type: Date },
    deleted_by: { type: String },
  }),
  "data_pic_v2"
);
module.exports = PIC;
