const mongoose = require("mongoose");
const database = require("../../config/database");

const picSchema = mongoose.Schema(
  {
    name: { type: String, default: "" },
    position: { type: String, default: "" },
    phone: { type: mongoose.Schema.Types.Mixed, default: "" },
    email: { type: String, default: "" },
  },
  { _id: false }
);

const Customer = database.model(
  "Customer V2",
  new mongoose.Schema({
    customer: { type: String, required: true },
    country: { type: String, required: true },
    email: { type: String, required: true },
    address: { type: String, required: true },
    top: { type: mongoose.Schema.Types.Mixed, required: true },
    remarks: { type: String, default: "" },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
    deleted_at: { type: Date },
    deleted_by: { type: String },
  }),
  "data_customer_v2"
);
module.exports = Customer;
