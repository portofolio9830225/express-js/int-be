const mongoose = require("mongoose");
const database = require("../../../config/database");

const masterProduct = database.model(
    'Master Products',
    new mongoose.Schema({
        product_mid: { type: String, default: "" },
        product_category: { type: String, default: "" },
        product_code: { type: String, default: "" },
        product_name: { type: String, default: "" },
        is_active: {type: Boolean},
        created_at: { type: Date },
        requester: { type: String }
    }), 'master_product'
);

module.exports = masterProduct