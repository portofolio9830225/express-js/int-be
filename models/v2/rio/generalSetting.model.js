const mongoose = require("mongoose");
const database = require("../../../config/database");

const generalSetting = database.model(
    'General Setting',
    new mongoose.Schema({
        key: String,
        value: Array,
        remark: Object,
        is_active: Boolean
    }), 'general_settings'
);

module.exports = generalSetting;