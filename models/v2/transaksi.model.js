const mongoose = require("mongoose");
const database = require("../../config/database");

const Transaksi = database.model(
  "Transaksi V2",
  new mongoose.Schema({
    id_customer: { type: mongoose.Schema.Types.ObjectId, required: true },
    date: { type: mongoose.Schema.Types.Mixed, default: "" },
    product: { type: mongoose.Schema.Types.Mixed, default: "" },
    packaging: { type: mongoose.Schema.Types.Mixed, default: "" },
    fob_price: { type: mongoose.Schema.Types.Mixed, default: "" },
    qty: { type: mongoose.Schema.Types.Mixed, default: "" },
    sales_person: { type: String, default: "" },
    remarks: { type: String, default: "" },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
    deleted_at: { type: Date },
    deleted_by: { type: String },
  }),
  "transaksi_v2"
);
module.exports = Transaksi;
