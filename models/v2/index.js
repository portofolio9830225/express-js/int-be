const database = require("../../config/database");
database.Promise = global.Promise;

const db = {};
db.mongoose = database;

// add here
db.customer = require("./customer.model");
db.pic = require("./pic.model");
db.transaksi = require("./transaksi.model");
db.transaksi_a = require("./azhar/transaction.model");
db.product = require("./azhar/product.model");
db.customer_a = require("./azhar/customer.model");
db.product_a = require("./azhar/product.model");

// rio
db.file = require('../file.model');
db.master_customers = require('./rio/masterCustomer.model');
db.master_products = require('./rio/masterProduct.model');
db.transactions = require('./rio/transaction.model');
db.general_settings = require('./rio/generalSetting.model');

module.exports = db;
