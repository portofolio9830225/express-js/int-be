const mongoose = require("mongoose");
const File = mongoose.model(
  "File",
  new mongoose.Schema({
    onedrive_id: { type: Object, required: true },
    is_active: { type: String, required: true },
    created_at: { type: Date },
    originalname: { type: String, required: true },
    apps: { type: String, required: true },
    requester: { type: String },
    mimetype: { type: String, required: true },
    type: { type: String, required: true },
  }),
  "file"
);
module.exports = File;
