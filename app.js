const app = require("./server");
const pjs = require("./package.json");
const debug = require("debug")("backend-int:server");
// START SERVER
server = app.listen(process.env.PORT || 0, () => {
  debug(
    `${pjs.name} v${pjs.version} listening on ${server.address().address}:${
      server.address().port
    }...`
  );
});
