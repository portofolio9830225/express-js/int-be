const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-int:data-customer-module");
const helper = require("../../common/helper");
const db = require("../../models");
const DataCustomer = db.data_customer;
const OneTimeScript = db.one_time_script;
const zeroPad = require("../../helpers/zeroPad");
const moment = require("moment");
const escapeRegex = require("../../common/escapeRegex");
const UserModule = require("../userModule");

const createSchema = Joi.object({
  requester: Joi.string().required(),
  department: Joi.string().required(),
  data_customer: Joi.object().required(),
});

const createSchemaBulk = Joi.object({
  requester: Joi.string().required(),
  department: Joi.string().required(),
  list_customer: Joi.array().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}

function validateCreateSchemaBulk(schema) {
  return createSchemaBulk.validate(schema);
}

async function create(payload) {
  try {
    debug(payload, "===> PARAMS");

    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const requester = payload.requester;
    const department = payload.department;
    const data_customer = payload.data_customer;

    let dataCustomer = await DataCustomer.find({
      is_active: true,
      department,
    });

    const new_data_customer = { ...data_customer };
    new_data_customer.is_active = true;
    new_data_customer.requester = requester;
    new_data_customer.department = department;
    new_data_customer.created_at = new Date();
    new_data_customer.created_by = requester;
    new_data_customer.version = `DC-${
      moment().format("YYYYMMDDHHmmss") +
      zeroPad.zeroPad(dataCustomer.length + 1, 4)
    }`;

    const result = await new DataCustomer(new_data_customer).save();

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function createBulk(payload) {
  try {
    debug(payload, "===> PARAMS BULK");

    const validate = validateCreateSchemaBulk(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const requester = payload.requester;
    const department = payload.department;
    const list_customer = payload.list_customer;

    let dataOneTimeScript = await OneTimeScript.find({
      is_active: true,
      department,
    });

    // init ots
    const one_time_script = {};
    one_time_script.is_active = true;
    one_time_script.department = department;
    one_time_script.created_at = new Date();
    one_time_script.created_by = requester;
    one_time_script.version = `EO-${
      moment().format("YYYYMMDDHHmmss") +
      zeroPad.zeroPad(dataOneTimeScript.length + 1, 4)
    }`;

    let dataCustomer = await DataCustomer.find({
      is_active: true,
      department,
    });

    // init data_customer
    const data_customer = [];

    list_customer.forEach((item, index) => {
      const new_data_customer = { ...item };
      new_data_customer.is_active = true;
      new_data_customer.department = department;
      new_data_customer.created_at = new Date();
      new_data_customer.created_by = requester;
      new_data_customer.version_ots = one_time_script.version;
      new_data_customer.version = `DC-${
        moment().format("YYYYMMDDHHmmss") +
        zeroPad.zeroPad(dataCustomer.length + index + 1, 4)
      }`;

      data_customer.push(new_data_customer);
    });

    debug(data_customer, "===> PARAMS INSERT MANY");

    let resultManny = await DataCustomer.insertMany(data_customer);
    let result = await new OneTimeScript(one_time_script).save();

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function read(query) {
  try {
    // CHECK PARAMS
    if (!query.requester) {
      return error.errorReturn({ message: "requester is required" });
    }
    if (!query.department) {
      return error.errorReturn({ message: "department is required" });
    }

    // QUERY
    let condition = { is_active: true };

    // STRING -- REGEX
    if (query.filterBy === "requester") {
      condition.created_by = new RegExp(escapeRegex(query.search), "i");
    }
    if (query.filterBy === "version") {
      condition.version = new RegExp(escapeRegex(query.search), "i");
    }
    if (query.version_ots) {
      condition.version_ots = new RegExp(escapeRegex(query.version_ots), "i");
    }
    if (query.department) {
      condition.department = new RegExp(escapeRegex(query.department), "i");
    }

    // init filter sales_person
    let list_person = [];
    list_person.push(query.requester);

    // init params list under level
    const params_under_level = {
      nik: query.requester,
      access_token: query.access_token,
    };

    // get list under level
    const result_under_level = await UserModule.getUnderLevel(
      params_under_level
    );

    // check result under level
    if (!result_under_level.error) {
      const list = result_under_level.map((item) => item.nik);
      list_person = list_person.concat(list);
    }

    // check value sales person
    if (list_person.length !== 0) {
      // append filter sales_person
      condition = {
        ...condition,
        $or: [
          { sales_person: { $in: list_person } },
          { created_by: query.requester },
        ],
      };
    }

    debug(condition, "===> PARAMS READ");

    // SORT by ASC or DESC
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "created_at";
    const sort = query.sortBy ? { [sortField]: sortOrder } : { created_at: -1 };

    //SKIP
    const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;

    //LIMIT
    const limit = sizePerPage;

    const dataCustomer = await DataCustomer.find(condition)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyObject(dataCustomer)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await DataCustomer.find(condition);
    const result = {
      foundData: dataCustomer,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };
    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function readBulk(query) {
  try {
    // CHECK PARAMS
    if (!query.requester) {
      return error.errorReturn({ message: "requester is required" });
    }
    if (!query.department) {
      return error.errorReturn({ message: "department is required" });
    }

    // QUERY
    let condition = { is_active: true };

    // STRING -- REGEX
    if (query.filterBy === "version") {
      condition.version = new RegExp(escapeRegex(query.search), "i");
    }
    if (query.department) {
      condition.department = new RegExp(escapeRegex(query.department), "i");
    }

    // init person view
    let list_person = [];
    list_person.push(query.requester);

    // init params list under level
    const params_under_level = {
      nik: query.requester,
      access_token: query.access_token,
    };

    // get list under level
    const result_under_level = await UserModule.getUnderLevel(
      params_under_level
    );

    // check result under level
    if (!result_under_level.error) {
      const list = result_under_level.map((item) => item.nik);
      list_person = list_person.concat(list);
    }

    // check value sales person
    if (list_person.length !== 0) {
      // append filter sales_person
      condition.created_by = { $in: list_person };
    }

    debug(condition, "===> PARAMS READ BULK");

    // SORT by ASC or DESC
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "created_at";
    const sort = query.sortBy ? { [sortField]: sortOrder } : { created_at: -1 };

    //SKIP
    const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;

    //LIMIT
    const limit = sizePerPage;

    const dataCustomer = await OneTimeScript.find(condition)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyObject(dataCustomer)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await OneTimeScript.find(condition);
    const result = {
      foundData: dataCustomer,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };
    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  create,
  createBulk,
  read,
  readBulk,
};
