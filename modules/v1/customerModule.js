const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-int:data:customer-module");
const db = require("../../models");
const helper = require("../../common/helper");
const escapeRegex = require("../../common/escapeRegex");
const { ObjectId } = require("mongodb");
const { getUnderLevel } = require("../userModule");
const Excel = require("excel4node");

const Customer = db.customer;
const historyModule = require("../historyModule");

const createSchema = Joi.object().keys({
  contact_person: Joi.array(),
  contact_detail: Joi.array(),
  details: Joi.array(),
  customer_name: Joi.string(),
  country: Joi.string(),
  top: Joi.string().allow(""),
  comercial_lost: Joi.number(),
  qty_year_1: Joi.number(),
  qty_year_2: Joi.number(),
  sales_person: Joi.string().allow(""),
  department: Joi.string(),
  requester: Joi.string(),
});
const createBulkSchema = Joi.object({
  requester: Joi.string().required(),
  department: Joi.string().required(),
  data: Joi.array().min(1).required(),
});
const updateSchema = createSchema.keys({
  _id: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}
function validateCreateBulkSchema(schema) {
  return createBulkSchema.validate(schema);
}
function validateUpdateBulkSchema(schema) {
  return updateSchema.validate(schema);
}

async function create(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD CREATE");

    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    return {};
  } catch (e) {
    debug(e);
    debug("===> ERROR CREATE");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

async function createBulk(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD CREATE BULK");

    const validate = validateCreateBulkSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const requester = payload.requester;
    const department = payload.department;
    const data = payload.data;

    const new_bulk = data.map((item) => {
      const new_value = { ...item };
      new_value.is_active = true;
      new_value.department = department;
      new_value.updated_at = new Date();
      new_value.updated_by = requester;

      const new_update = {
        updateOne: {
          filter: {
            customer_name: item.customer_name,
            country: item.country,
            qty_year_2: item.qty_year_2,
          },
          update: {
            $set: { ...new_value },
            $setOnInsert: {
              created_at: new Date(),
              created_by: requester,
            },
          },
          upsert: true,
        },
      };

      return new_update;
    });

    const result = await Customer.bulkWrite(new_bulk);

    if (result?.error) {
      result;
    }

    debug(result.result.upserted);

    return { error: false, data: result.result.upserted };
  } catch (e) {
    debug(e);
    debug("===> ERROR CREATE BULK");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

async function read(query) {
  try {
    debug(query);
    debug("===> QUERY READ");

    let dataUsers = [];

    if (query?.requester) {
      const underLevel = await getUnderLevel({
        id: query.requester,
        user_token: query.user_token,
      });
      if (!underLevel?.error) {
        dataUsers = underLevel.map((item) => item.nik);
      }
      dataUsers.push(query.requester);
      debug(dataUsers);
      debug("===> FILTER USERS");
    }

    // query
    let filter = {
      is_active: true,
      $or: [
        { created_by: { $in: [...dataUsers] } },
        { sales_person: { $in: [...dataUsers] } },
      ],
    };
    if (query?.description) {
      filter = {
        ...filter,
        $or: [
          { customer_name: { $regex: query.description, $options: "i" } },
          { country: { $regex: query.description, $options: "i" } },
        ],
      };
    }
    if (query?.year) {
      filter = { ...filter, qty_year_1: Number(query.year) };
    }

    debug(filter);
    debug("===> FILTER READ");

    // sort
    const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query?.sortField ?? "created_at";
    const sort = { [sortField]: sortOrder };

    // skip
    const sizePerPage = query?.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query?.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;

    // limit
    const limit = sizePerPage;

    // find data
    let found_data = [];

    if (query?.showAll) {
      found_data = await Customer.find(filter)
        .sort(sort)
        .collation({ locale: "en_US", numericOrdering: true })
        .select("-__v")
        .lean();
    } else {
      found_data = await Customer.find(filter)
        .sort(sort)
        .collation({ locale: "en_US", numericOrdering: true })
        .skip(skip)
        .limit(limit)
        .select("-__v")
        .lean();
    }

    // if empty data
    if (helper.isEmptyArray(found_data)) {
      const result = {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
      debug(result);
      debug("===> RESULT READ");
      return result;
    }

    //
    const count_data = await Customer.countDocuments(filter).lean();
    const current_page = query.showAll ? 1 : page;
    const count_page = query.showAll ? 1 : Math.ceil(count_data / sizePerPage);
    const result = {
      foundData: found_data,
      currentPage: current_page,
      countPages: count_page,
      countData: count_data,
    };

    debug("COUNT : ", result?.foundData?.length);
    debug("===> RESULT READ");

    return result;
  } catch (e) {
    debug(e);
    debug("===> ERROR READ");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

async function update(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD UPDATE EXCEL");

    const validate = validateUpdateBulkSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const id = payload._id;
    const requester = payload.requester;

    delete payload._id;
    delete payload.requester;

    const old_filter = { is_active: true, _id: ObjectId(id) };
    let old_data = await Customer.findOne(old_filter).lean();
    let new_data = {
      ...payload,
      updated_at: new Date(),
      updated_by: requester,
    };

    const result = await Customer.findOneAndUpdate(
      { _id: ObjectId(id) },
      { ...new_data },
      { useFindAndModify: false, new: true }
    );

    debug(result);
    debug("===> RESULT UPDATE");

    if (helper.isEmptyObject(result)) {
      return error.errorReturn({
        message: "Failed to update data customer, data not found",
      });
    }

    if (result?.error) {
      return error.errorReturn({
        message: "Failed to update data customer",
      });
    }

    const new_history = {
      requester,
      id_history: result._id,
      collection_name: Customer.collection.collectionName,
      data: old_data,
    };

    await historyModule.create(new_history);

    return result;
  } catch (e) {
    debug(e);
    debug("===> ERROR UPDATE EXCEL");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

async function destroy(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD DELETE EXCEL");

    return [];
  } catch (e) {
    debug(e);
    debug("===> ERROR DELETE EXCEL");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

async function generateExcel(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD GENERATE EXCEL");

    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet("Data Customer");

    const buffer = workbook.writeToBuffer();

    return buffer;
  } catch (e) {
    debug(e);
    debug("===> ERROR GENERATE EXCEL");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

async function readExcel(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD READ EXCEL");

    return [];
  } catch (e) {
    debug(e);
    debug("===> ERROR READ EXCEL");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

module.exports = {
  create,
  createBulk,
  read,
  update,
  destroy,
  generateExcel,
  readExcel,
};
