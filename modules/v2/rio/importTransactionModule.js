const error = require("../../../common/errorMessage");
const debug = require("debug")("backend-int:transaksi-module");
const db = require("../../../models");
const FormData = require("form-data");
const path = require("path");
const moment = require('moment');
const { readFileSync, unlinkSync } = require("fs");
const axios = require("axios");
const URL_FILE_SERVICE = "/api/file/onedrive/read-file/";
const MailModule = require('../../mailModule');

const generalSetting = db.general_settings;
const File = db.file;
const masterCustomer = db.master_customers;
const masterProduct = db.master_products;
const transactions = db.transactions;

async function upload(payload, currentUser) {
    try {
        payload.upload_data = JSON.parse(payload.upload_data);
        const uploadData = payload.upload_data;
    
        const findCustomers = await masterCustomer.find();
        const customers_code = [];
        findCustomers.forEach((customer) => {
            customers_code.push(customer.customer_code);
        });
    
        const findProducts = await masterProduct.find();
        const products_code = [];
        findProducts.forEach((product) => {
            products_code.push(product.product_code);
        });
    
        const customers_payload = [];
        const products_payload = [];
        uploadData.forEach((data) => {
            if (!customers_code.includes(data.customer_code.toString().toUpperCase())) {
                customers_code.push(data.customer_code.toString().toUpperCase());
                customers_payload.push({
                  customer_code: data.customer_code.toString().toUpperCase(),
                  customer_name: data.customer_name,
                  is_active: true,
                  created_at: new Date().toISOString(),
                  requester: currentUser.username,
                });
            }
    
            if (!products_code.includes(data.product_code.toString().toUpperCase())) {
                products_code.push(data.product_code.toString().toUpperCase());
                products_payload.push({
                  product_mid: data.product_category_1,
                  product_category: data.product_category_2,
                  product_code: data.product_code.toString().toUpperCase(),
                  product_name: data.product_name,
                  is_active: true,
                  created_at: new Date().toISOString(),
                  requester: currentUser.username,
                });
            }
        });
        const masterCustomerResult = await masterCustomer.insertMany(customers_payload);
        const masterProductResult = await masterProduct.insertMany(products_payload);
        
        const transactionResult = await transactions.insertMany(uploadData);
        
        if (payload.files.length > 0) {
        if (payload.files[0].size / (1024 * 1024) > 2) {
            return error.errorReturn({ message: "Max file size 2MB" });
          }
          let parameters = {
            method: "POST",
            url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
            headers: {
              "x-application-token": `${payload.app_token}`,
              "x-public-key": `${process.env.PUBLIC_KEY}`,
            },
          };
      
          const form = new FormData();
          form.append("apps", process.env.NAME);
      
          //upload files to onedrive, get the link
          payload.files.forEach((f) => {
            const readDir = path.join(__dirname, `../../../${f.path}`);
            const readFile = readFileSync(readDir);
            form.append("upload_file", readFile, f.path);
          });
          parameters.data = form;
          parameters.maxContentLength = Infinity;
          parameters.maxBodyLength = Infinity;
          parameters.headers["Content-Type"] =
            "multipart/form-data;boundary=" + form.getBoundary();
      
          const result = await axios(parameters)
            .then(function async(response) {
              return response.data;
            })
            .catch(function (error) {
              debug(error, "=> error callApiByArea");
              return [];
            });

          const OneDriveId = result[0].onedrive_id;
      
          for (let f of payload.files) {
            // remove files when finish make call
            unlinkSync(path.join(__dirname, `../../../${f.path}`));
          }

          let filePayload = {
            onedrive_id: OneDriveId,
            originalname: result[0].originalname,
            mimetype: result[0].mimetype,
            apps: result[0].apps,
            created_at: new Date().toISOString(),
            is_active: true,
            type: 'import_transaksi'
          };
          const fileResult = await new File(filePayload).save();

          if (payload.isNotify === 'true') {
            const getNotifyUser = await generalSetting.findOne({key: 'mail_notification_import'});
            const body = `<p>Dear All,</p><br><p>Dokumen transaksi International Sales telah diimport oleh ${currentUser.username} pada ${moment(new Date().toISOString()).format('YYYY-MM-DD hh:mm')}</p>`;
            const send = {
              subject: "Import",
              body: body,
              send_to: getNotifyUser?.value,
              send_cc: [],
            };
            await MailModule.sendEmail(
              send.subject,
              send.body,
              send.send_to,
              send.send_cc
            );
          }

          return {
            data: [{
                master_customers: masterCustomerResult,
                master_products: masterProductResult,
                transaction: transactionResult,
                file: fileResult,
            }],
            status: 'Data has been added successfully'
          };
      }
    } catch (error) {
        debug(error);
    }
};

async function readHistory() {
    try {
        const filter = {is_active: true, type: 'import_transaksi'};
        const fileData = await File.find(filter).sort({created_at: -1}).limit(5);
        return fileData;
    } catch (error) {
        debug(error)
    }
}

async function downloadFile(payload) {
  let id = "";
  Object.entries(payload.id).forEach(([key, val]) => {
    id = key;
  });
  let parameters = {
    method: "GET",
    url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}${URL_FILE_SERVICE}?onedrive_id=${id}`,
    headers: {
      "x-application-token": `${payload.app_token}`,
      "x-public-key": `${process.env.PUBLIC_KEY}`,
    },
  };
  const res = await axios(parameters)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      debug(e);
    });
  debug(res);
  return { res };
}

module.exports = { upload, readHistory, downloadFile }