const debug = require("debug")("backend-int:data:customer-module");
const db = require("../../../models");

const Product_a = db.product_a;

async function readAll() {
	try {
		const data = await Product_a.find();
		return data;
	} catch (e) {
		debug(e);
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

module.exports = { readAll };
