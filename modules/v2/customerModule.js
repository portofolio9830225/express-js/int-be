const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-int:data:customer-module");
const db = require("../../models");
const helper = require("../../common/helper");
const escapeRegex = require("../../common/escapeRegex");
const { ObjectId } = require("mongodb");
const { getUnderLevel } = require("../userModule");
const historyModule = require("../historyModule");

const picModule = require("./picModule");
const transaksiModule = require("./transaksiModule");

const Customer = db.customer;
const Customer_a = db.customer_a;

const createSchema = Joi.object().keys({
	requester: Joi.string().required(),
	department: Joi.string().required(),
	customer: Joi.string().required(),
	country: Joi.string().required(),
	email: Joi.string().required(),
	address: Joi.string().required(),
	top: Joi.any(),
});

const createBulkSchema = Joi.object().keys({
	requester: Joi.string().required(),
	department: Joi.string().required(),
	data_customer: Joi.array()
		.min(1)
		.items(
			Joi.object({
				customer: Joi.string(),
				country: Joi.string(),
				email: Joi.string(),
				address: Joi.string(),
				top: Joi.any(),
			})
		)
		.required(),
	data_pic: Joi.array()
		.items(
			Joi.object({
				customer: Joi.string(),
				name: Joi.string(),
				position: Joi.string(),
				phone: Joi.any(),
				email: Joi.string(),
			})
		)
		.required(),
	data_transaksi: Joi.array()
		.items(
			Joi.object({
				customer: Joi.string(),
				date: Joi.any(),
				product: Joi.any(),
				packaging: Joi.any(),
				fob_price: Joi.any(),
				qty: Joi.any(),
				sales_person: Joi.string(),
			})
		)
		.required(),
});

const updateSchema = createSchema.keys({
	_id: Joi.string().required(),
});

const deleteSchema = Joi.object().keys({
	_id: Joi.string().required(),
	requester: Joi.string().required(),
	remarks: Joi.string().required(),
});

function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}

function validateCreateBulkSchema(schema) {
	return createBulkSchema.validate(schema);
}

function validateUpdateSchema(schema) {
	return updateSchema.validate(schema);
}

function validateDeleteSchema(schema) {
	return deleteSchema.validate(schema);
}

async function create(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD CREATE");

		const validate = validateCreateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const requester = payload.requester;
		delete payload.requester;

		const new_values = { ...payload };
		new_values.is_active = true;
		new_values.created_at = new Date();
		new_values.created_by = requester;

		const result = await new Customer(new_values).save();

		if (result?.error) {
			debug(result?.error);
			debug("===> RESULT ERROR CREATE");
			return error.errorReturn({ message: result?.message });
		}

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR CREATE");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function createBulk(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD CREATE BULK");

		const validate = validateCreateBulkSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const new_values = [...payload.data_customer];

		const new_result = [];

		// init data update or create new document
		for (let i = 0; i < new_values.length; i++) {
			const findExists = await Customer.findOne({
				is_active: true,
				customer: new_values[i].customer,
				country: new_values[i].country,
				created_by: payload.requester,
			});

			// create new document if it not already exists
			if (helper.isEmptyObject(findExists)) {
				const new_payload = {
					requester: payload.requester,
					department: payload.department,
					...new_values[i],
				};
				const result = await create(new_payload);
				if (result?.error || helper.isEmptyObject(result)) {
					// handle error
				} else {
					new_result.push(result);
				}
			}
			// update document if it already exists
			else if (!findExists?.error) {
				const new_payload = {
					requester: payload.requester,
					department: payload.department,
					_id: findExists._id?.toString(),
					...new_values[i],
				};
				const result = await update(new_payload);
				if (result?.error || helper.isEmptyObject(result)) {
					// handle error
				} else {
					new_result.push(result);
				}
			}
			//
			else {
				// handle error
			}
		}

		debug(new_result);
		debug("===> new_result");

		const new_pic = payload.data_pic.map(item => {
			const findPic = new_result.find(find => find.customer === item.customer);

			delete item.customer;

			return {
				requester: payload.requester,
				department: payload.department,
				id_customer: findPic?._id?.toString(),
				...item,
			};
		});

		debug(new_pic);
		debug("===> new_pic");

		// run bulk pic
		if (new_pic?.length > 0) {
			await picModule.createBulk(new_pic);
		}

		const new_transaksi = payload.data_transaksi.map(item => {
			const findCustomer = new_result.find(find => find.customer === item.customer);

			delete item.customer;

			return {
				requester: payload.requester,
				department: payload.department,
				id_customer: findCustomer?._id?.toString(),
				...item,
			};
		});

		debug(new_transaksi);
		debug("===> new_transaksi");

		// run bulk transaksi
		if (new_transaksi?.length > 0) {
			await transaksiModule.createBulk(new_transaksi);
		}

		// return if successful
		return { message: "Data has been processed successfully" };
	} catch (e) {
		debug(e);
		debug("===> ERROR CREATE BULK");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function read(query) {
	try {
		debug(query);
		debug("===> QUERY READ");

		let filter = { is_active: true };

		// filter by requester
		if (query?.requester) {
			filter = { ...filter, created_by: { $in: query?.requester } };
		}

		// filter by underlevel user
		if (query?.requester && query.underLevel) {
			let listUsers = [];

			const findUnderLevel = await getUnderLevel({
				id: query.requester,
				user_token: query.user_token,
			});

			if (!findUnderLevel?.error) {
				listUsers = findUnderLevel.map(item => item?.nik);
			}

			listUsers.push(query.requester);

			filter = { ...filter, created_by: { $in: listUsers } };
		}

		// filter by description
		if (query?.description) {
			filter = {
				...filter,
				$or: [
					{ customer: { $regex: query.description, $options: "i" } },
					{ country: { $regex: query.description, $options: "i" } },
					{ email: { $regex: query.description, $options: "i" } },
					{ address: { $regex: query.description, $options: "i" } },
				],
			};
		}

		// sort
		const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query?.sortField ?? "created_at";
		const sort = { [sortField]: sortOrder };

		// skip
		const sizePerPage = query?.sizePerPage ? Number(query.sizePerPage) : 10;
		const page = query?.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;

		// limit
		const limit = sizePerPage;

		// find data
		let found_data = [];

		if (query?.showAll) {
			found_data = await Customer.find(filter)
				.sort(sort)
				.collation({ locale: "en_US", numericOrdering: true })
				.select("-__v -is_active")
				.lean();
		} else {
			found_data = await Customer.find(filter)
				.sort(sort)
				.collation({ locale: "en_US", numericOrdering: true })
				.skip(skip)
				.limit(limit)
				.select("-__v -is_active")
				.lean();
		}

		// if empty data
		if (helper.isEmptyArray(found_data)) {
			const result = {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
			debug(result);
			debug("===> RESULT READ");
			return result;
		}

		//
		const count_data = await Customer.countDocuments(filter).lean();
		const current_page = query.showAll ? 1 : page;
		const count_page = query.showAll ? 1 : Math.ceil(count_data / sizePerPage);
		const result = {
			foundData: found_data,
			currentPage: current_page,
			countPages: count_page,
			countData: count_data,
		};

		debug("COUNT : ", result?.foundData?.length);
		debug("===> RESULT READ");

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR READ");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function update(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD UPDATE");

		const validate = validateUpdateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const id = payload._id;
		const requester = payload.requester;

		delete payload._id;
		delete payload.requester;

		const old_filter = { is_active: true, _id: ObjectId(id) };
		let old_data = await Customer.findOne(old_filter).lean();
		let new_data = {
			...payload,
			updated_at: new Date(),
			updated_by: requester,
		};

		const result = await Customer.findOneAndUpdate(
			{ _id: ObjectId(id) },
			{ ...new_data },
			{ useFindAndModify: false, new: true }
		);

		if (result?.error || helper.isEmptyObject(result)) {
			return error.errorReturn({ message: result?.message ?? "update failed" });
		}

		const new_history = {
			requester,
			id_history: result._id,
			collection_name: Customer.collection.collectionName,
			data: old_data,
		};

		await historyModule.create(new_history);

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR UPDATE");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function destroy(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD DELETE");

		const validate = validateDeleteSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		return {};
	} catch (e) {
		debug(e);
		debug("===> ERROR DELETE");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function readAll() {
	try {
		const data = await Customer_a.find();
		return data;
	} catch (e) {
		debug(e);
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function store(payload) {
	try {
		let create = { ...payload };
		delete create["two_phase_country"];
		delete create["two_phase_region"];
		delete create["three_phase_country"];
		delete create["three_phase_region"];

		create["2_phase_country"] = payload.two_phase_country;
		create["2_phase_region"] = payload.two_phase_region;
		create["3_phase_country"] = payload.three_phase_country;
		create["3_phase_region"] = payload.three_phase_region;

		const data = await Customer_a.create(create);
		debug(data);

		return "success";
	} catch (e) {
		debug(e);
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function readTable(query) {
	try {
		debug("readTable ==>", query);

		let condition = {};

		if (query.type) {
			condition[query.type] = { $regex: ".*" + query.keyword + ".*", $options: "i" };
		}

		const sizePerPage = query.perPage ? Number(query.perPage) : 10;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;

		const data = await Customer_a.find(condition).sort({ _id: -1 }).skip(skip).limit(sizePerPage);
		const numFound = await Customer_a.find(condition);

		return {
			data: data,
			countData: Object.keys(numFound).length,
			currentPage: page,
			countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
		};
	} catch (e) {
		debug(e);
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function deleteById(payload) {
	try {
		await Customer_a.deleteOne({ _id: payload.idCustomer });
		return "success";
	} catch (e) {
		debug(e);
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function updateById(payload) {
	try {
		let new_data = { ...payload };
		delete new_data["_id"];
		delete new_data["two_phase_country"];
		delete new_data["two_phase_region"];
		delete new_data["three_phase_country"];
		delete new_data["three_phase_region"];

		new_data["2_phase_country"] = payload.two_phase_country;
		new_data["2_phase_region"] = payload.two_phase_region;
		new_data["3_phase_country"] = payload.three_phase_country;
		new_data["3_phase_region"] = payload.three_phase_region;

		debug(new_data);

		await Customer_a.findOneAndUpdate({ _id: ObjectId(payload._id) }, new_data);

		return "success";
	} catch (e) {
		debug(e);
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

module.exports = { create, createBulk, read, update, destroy, readAll, store, readTable, deleteById, updateById };
