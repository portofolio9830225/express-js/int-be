const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-int:data:pic-module");
const db = require("../../models");
const helper = require("../../common/helper");
const escapeRegex = require("../../common/escapeRegex");
const { ObjectId } = require("mongodb");
const historyModule = require("../historyModule");

const PIC = db.pic;

const createSchema = Joi.object().keys({
  requester: Joi.string().required(),
  department: Joi.string().required(),
  id_customer: Joi.string().required(),
  name: Joi.string(),
  position: Joi.string(),
  phone: Joi.any(),
  email: Joi.string(),
});

const createBulkSchema = Joi.array().required();

const updateSchema = createSchema.keys({
  _id: Joi.string().required(),
});

const deleteSchema = Joi.object().keys({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  remarks: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}

function validateCreateBulkSchema(schema) {
  return createBulkSchema.validate(schema);
}

function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}

function validateDeleteSchema(schema) {
  return deleteSchema.validate(schema);
}

async function create(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD CREATE");

    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const requester = payload.requester;
    delete payload.requester;

    const new_values = { ...payload };
    new_values.is_active = true;
    new_values.created_at = new Date();
    new_values.created_by = requester;

    const result = await new PIC(new_values).save();

    if (result?.error) {
      debug(result?.error);
      debug("===> RESULT ERROR CREATE");
      return error.errorReturn({ message: result?.message });
    }

    debug(result);
    debug("===> RESULT CREATE");

    return result;
  } catch (e) {
    debug(e);
    debug("===> ERROR CREATE");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function createBulk(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD CREATE BULK");

    const validate = validateCreateBulkSchema(payload);
    if (validate.error) {
      debug(validate);
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const new_result = [];

    payload.forEach(async (item) => {
      const findExists = await PIC.findOne({
        is_active: true,
        id_customer: ObjectId(item.id_customer),
        name: item.name,
        position: item.position,
      });

      // create new document if it not already exists
      if (helper.isEmptyObject(findExists)) {
        //
        const result = await create(item);
        if (result?.error || helper.isEmptyObject(result)) {
          // handle error
        } else {
          new_result.push(result);
        }
      }
      // update document if it already exists
      else if (!findExists?.error) {
        const result = await update({
          ...item,
          _id: findExists?._id?.toString(),
        });
        if (result?.error || helper.isEmptyObject(result)) {
          // handle error
        } else {
          new_result.push(result);
        }
      }
      //
      else {
        // handle error
      }
    });

    return {};
  } catch (e) {
    debug(e);
    debug("===> ERROR CREATE BULK");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function read(query) {
  try {
    debug(query);
    debug("===> QUERY READ");

    let filter = { is_active: true };

    if (query?.id_customer) {
      filter = { ...filter, id_customer: ObjectId(query.id_customer) };
    }

    // sort
    const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query?.sortField ?? "name";
    const sort = { [sortField]: sortOrder };

    // skip
    const sizePerPage = query?.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query?.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;

    // limit
    const limit = sizePerPage;

    // find data
    let found_data = [];

    if (query?.showAll) {
      found_data = await PIC.find(filter)
        .sort(sort)
        .collation({ locale: "en_US", numericOrdering: true })
        .select("-__v -created_at -created_by -is_active")
        .lean();
    } else {
      found_data = await PIC.find(filter)
        .sort(sort)
        .collation({ locale: "en_US", numericOrdering: true })
        .skip(skip)
        .limit(limit)
        .select("-__v -created_at -created_by -is_active")
        .lean();
    }

    // if empty data
    if (helper.isEmptyArray(found_data)) {
      const result = {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
      debug(result);
      debug("===> RESULT READ");
      return result;
    }

    //
    const count_data = await PIC.countDocuments(filter).lean();
    const current_page = query.showAll ? 1 : page;
    const count_page = query.showAll ? 1 : Math.ceil(count_data / sizePerPage);
    const result = {
      foundData: found_data,
      currentPage: current_page,
      countPages: count_page,
      countData: count_data,
    };

    debug("COUNT : ", result?.foundData?.length);
    debug("===> RESULT READ");

    return result;
  } catch (e) {
    debug(e);
    debug("===> ERROR READ");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function update(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD UPDATE");

    const validate = validateUpdateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const id = payload._id;
    const requester = payload.requester;

    delete payload._id;
    delete payload.requester;

    const old_filter = { is_active: true, _id: ObjectId(id) };
    let old_data = await PIC.findOne(old_filter).lean();
    let new_data = {
      ...payload,
      updated_at: new Date(),
      updated_by: requester,
    };

    const result = await PIC.findOneAndUpdate(
      { _id: ObjectId(id) },
      { ...new_data },
      { useFindAndModify: false, new: true }
    );

    if (result?.error || helper.isEmptyObject(result)) {
      return error.errorReturn({ message: result?.message ?? "update failed" });
    }

    const new_history = {
      requester,
      id_history: result._id,
      collection_name: PIC.collection.collectionName,
      data: old_data,
    };

    await historyModule.create(new_history);

    return result;
  } catch (e) {
    debug(e);
    debug("===> ERROR UPDATE");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function destroy(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD DELETE");

    const validate = validateDeleteSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    return {};
  } catch (e) {
    debug(e);
    debug("===> ERROR DELETE");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

module.exports = { create, createBulk, read, update, destroy };
