const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-int:transaksi-module");
const db = require("../../models");
const helper = require("../../common/helper");
const escapeRegex = require("../../common/escapeRegex");
const { ObjectId } = require("mongodb");
const historyModule = require("../historyModule");
const moment = require("moment");

const Transaksi = db.transaksi;
const Transaksi_a = db.transaksi_a;
const Customer_a = db.customer_a;
const Product_a = db.product_a;

const createSchema = Joi.object().keys({
	requester: Joi.string().required(),
	department: Joi.string().required(),
	id_customer: Joi.string().required(),
	date: Joi.any(),
	product: Joi.any(),
	packaging: Joi.any(),
	fob_price: Joi.any(),
	qty: Joi.any(),
	sales_person: Joi.string(),
});

const createBulkSchema = Joi.array().required();

const updateSchema = createSchema.keys({
	_id: Joi.string().required(),
});

const deleteSchema = Joi.object().keys({
	_id: Joi.string().required(),
	requester: Joi.string().required(),
	remarks: Joi.string().required(),
});

function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}

function validateCreateBulkSchema(schema) {
	return createBulkSchema.validate(schema);
}

function validateUpdateSchema(schema) {
	return updateSchema.validate(schema);
}

function validateDeleteSchema(schema) {
	return deleteSchema.validate(schema);
}

async function create(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD CREATE");

		const validate = validateCreateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const requester = payload.requester;
		delete payload.requester;

		const new_values = { ...payload };
		new_values.date = helper.getFormatDate(payload.date);
		new_values.is_active = true;
		new_values.created_at = new Date();
		new_values.created_by = requester;

		const result = await new Transaksi(new_values).save();

		if (result?.error) {
			debug(result?.error);
			debug("===> RESULT ERROR CREATE");
			return error.errorReturn({ message: result?.message });
		}

		debug(result);
		debug("===> RESULT CREATE");

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR CREATE");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function createBulk(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD CREATE BULK");

		const validate = validateCreateBulkSchema(payload);
		if (validate.error) {
			debug(validate);
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		debug("===> RUN BULK");

		const new_result = [];

		payload.forEach(async item => {
			const findExists = await Transaksi.findOne({
				is_active: true,
				id_customer: ObjectId(item.id_customer),
				date: item.date,
				product: item.product,
				sales_person: item.sales_person,
			});

			debug(findExists);
			debug("===> findExists");

			// create new document if it not already exists
			if (helper.isEmptyObject(findExists)) {
				//
				const result = await create(item);
				if (result?.error || helper.isEmptyObject(result)) {
					// handle error
				} else {
					new_result.push(result);
				}
			}
			// update document if it already exists
			else if (!findExists?.error) {
				const result = await update({
					...item,
					_id: findExists?._id?.toString(),
				});
				if (result?.error || helper.isEmptyObject(result)) {
					// handle error
				} else {
					new_result.push(result);
				}
			}
			//
			else {
				// handle error
			}
		});

		return {};
	} catch (e) {
		debug(e);
		debug("===> ERROR CREATE BULK");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function read(query) {
	try {
		debug(query);
		debug("===> QUERY READ");

		let filter = { is_active: true };

		if (query?.id_customer) {
			filter = { ...filter, id_customer: ObjectId(query.id_customer) };
		}

		if (query?.year) {
			filter = { ...filter, year: query.year };
		}

		// sort
		const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query?.sortField ?? "date";
		const sort = { [sortField]: sortOrder };

		// skip
		const sizePerPage = query?.sizePerPage ? Number(query.sizePerPage) : 10;
		const page = query?.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;

		// limit
		const limit = sizePerPage;

		// find data
		let found_data = [];

		if (query?.showAll) {
			found_data = await Transaksi.find(filter)
				.sort(sort)
				.collation({ locale: "en_US", numericOrdering: true })
				.select("-__v -created_at -created_by -is_active")
				.lean();
		} else {
			found_data = await Transaksi.find(filter)
				.sort(sort)
				.collation({ locale: "en_US", numericOrdering: true })
				.skip(skip)
				.limit(limit)
				.select("-__v -created_at -created_by -is_active")
				.lean();
		}

		// if empty data
		if (helper.isEmptyArray(found_data)) {
			const result = {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
			debug(result);
			debug("===> RESULT READ");
			return result;
		}

		//
		const count_data = await Transaksi.countDocuments(filter).lean();
		const current_page = query.showAll ? 1 : page;
		const count_page = query.showAll ? 1 : Math.ceil(count_data / sizePerPage);
		const result = {
			foundData: found_data,
			currentPage: current_page,
			countPages: count_page,
			countData: count_data,
		};

		debug("COUNT : ", result?.foundData?.length);
		debug("===> RESULT READ");

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR READ");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function update(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD UPDATE");

		const validate = validateUpdateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const id = payload._id;
		const requester = payload.requester;

		delete payload._id;
		delete payload.requester;

		const old_filter = { is_active: true, _id: ObjectId(id) };
		let old_data = await Transaksi.findOne(old_filter).lean();
		let new_data = {
			...payload,
			date: helper.getFormatDate(payload.date),
			updated_at: new Date(),
			updated_by: requester,
		};

		debug(helper.getFormatDate(payload.date));
		debug("===> helper.getFormatDate(payload.date)");

		const result = await Transaksi.findOneAndUpdate(
			{ _id: ObjectId(id) },
			{ ...new_data },
			{ useFindAndModify: false, new: true }
		);

		if (result?.error || helper.isEmptyObject(result)) {
			return error.errorReturn({ message: result?.message ?? "update failed" });
		}

		const new_history = {
			requester,
			id_history: result._id,
			collection_name: Transaksi.collection.collectionName,
			data: old_data,
		};

		await historyModule.create(new_history);

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR UPDATE");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function destroy(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD DELETE");

		const validate = validateDeleteSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		return {};
	} catch (e) {
		debug(e);
		debug("===> ERROR DELETE");
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function readAdjust(query) {
	try {
		let condition = {};
		condition.$or = [{ customer_code: null }, { product_code: null }, { customer_code: "" }, { product_code: "" }];

		switch (query.category) {
			case "customer_code":
				condition.customer_code = { $nin: [null, ""], $regex: ".*" + query.keyword + ".*", $options: "i" };
				break;
			case "product":
				condition.product = { $regex: ".*" + query.keyword + ".*", $options: "i" };
				break;
			case "country":
				condition.country = { $regex: ".*" + query.keyword + ".*", $options: "i" };
				break;
			case "region":
				condition.region = { $regex: ".*" + query.keyword + ".*", $options: "i" };
				break;
			default:
				break;
		}

		if (query.start && query.end) {
			condition.created_at = {
				$gte: moment(query.start).startOf("day"),
				$lte: moment(query.end).startOf("day"),
			};
		}

		const perPages = query.perPages ? Number(query.perPages) : 10;
		const page = query.pages ? Number(query.pages) : 1;
		const skip = perPages * page - perPages;

		const data = await Transaksi_a.find(condition).skip(skip).limit(perPages);

		const numFound = await Transaksi_a.find(condition);

		return {
			data: data,
			countData: Object.keys(numFound).length,
			currentPage: page,
			countPages: Math.ceil(Object.keys(numFound).length / perPages),
		};
	} catch (e) {
		debug(e);
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

async function createAdjust(payload) {
	try {
		debug("==> CREATE ADJUST");
		debug(payload);

		payload.forEach(async (el, i) => {
			let update = {};
			const dt = await Transaksi_a.findById(el.id);

			if ((dt.customer_name == "" || dt.customer_name == null) && el.customer_name != "") {
				const customer = await Customer_a.findOne({ customer_name: el.customer_name });
				update.customer_name = customer.customer_name;
				update.customer_code = customer.customer_code;
			}

			if ((dt.product_code == "" || dt.product_code == null) && el.product_code != "") {
				const product = await Product_a.findOne({ product_code: el.product });
				update.product_code = product.product_code;
				update.product_name = product.product_name;
			}

			if (update != {}) {
				await Transaksi_a.findByIdAndUpdate(el.id, update);
			}
		});

		return { data: "success" };
	} catch (err) {
		debug(err);
		return error.errorReturn({ message: "Internal Server Error" });
	}
}

module.exports = { create, createBulk, read, update, destroy, readAdjust, createAdjust };
