const error = require("../common/errorMessage");
const debug = require("debug")("backend-int:log-module");
const jwt = require("jsonwebtoken");
const axios = require("axios");

async function sendLog(params) {
  try {
    const jwt_token = jwt.sign(
      {
        name: process.env.NAME,
        type: "private_apps",
      },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );
    const body = params.body;
    const parameters = {
      method: params.method,
      url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/log/`,
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
      },
      data: body,
    };

    const result = await axios(parameters);
    return result.data;
  } catch (err) {
    return { message: "Log Service is not found" };
  }
}

function getHostname(req) {
  if (req.ip.includes("::ffff:")) {
    return req.ip.replace("::ffff:", req.protocol + "://");
  } else if (req.ip.includes(":")) {
    return `${req.protocol}://[${req.ip}]`;
  } else {
    return `${req.protocol}://${req.ip}`;
  }
}
function getCallerIP(req) {
  let ip =
    req.headers["x-forwarded-for"] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress;

  ip = ip.split(",")[0];
  if (ip == "::1") {
    return `${req.protocol}://localhost`;
  } else if (ip.includes("::ffff:")) {
    return ip.replace("::ffff:", req.protocol + "://");
  } else if (ip.includes(":")) {
    return `${req.protocol}://[${ip}]`;
  } else {
    return `${req.protocol}://${req.ip}`;
  }
}
module.exports = {
  sendLog,
  getHostname,
  getCallerIP,
};
