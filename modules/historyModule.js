const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-int:history-module");
const db = require("../models");

const History = db.history;

const createSchema = Joi.object({
  requester: Joi.string().required(),
  id_history: Joi.object().required(),
  collection_name: Joi.string().required(),
  data: Joi.object().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}

async function create(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD CREATE HISTORY");

    const validate = validateCreateSchema(payload);
    if (validate?.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const requester = payload.requester;
    delete payload.requester;

    const new_values = { ...payload };
    new_values.created_at = new Date();
    new_values.created_by = requester;

    const result = await new History(new_values).save();

    debug(result);
    debug("===> RESULT CREATE HISTORY");

    return result;
  } catch (e) {
    debug(e);
    debug("===> ERROR CREATE HISTORY");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

module.exports = { create };
