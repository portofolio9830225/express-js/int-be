const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-int:menu-module");
const helper = require("../common/helper");
const db = require("../models");
const Submenu = require("../models/submenu.model");
const Menu = db.menu;
const { ObjectId } = require("mongodb");
const createSchema = Joi.object({
  name: Joi.string().required(),
  icon: Joi.string().required(),
  link: Joi.string().required(),
  isDeleted: Joi.boolean(),
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  name: Joi.string(),
  icon: Joi.string(),
  link: Joi.string(),
});

const idSchema = Joi.object({
  _id: Joi.string().required(),
});
function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}
function validateIdSchema(schema) {
  return idSchema.validate(schema);
}

async function createMenu(payload_params) {
  let payload = payload_params;

  const validate = validateCreateSchema(payload);
  if (validate.error) {
    return error.errorReturn({ message: validate.error.details[0].message });
  }
  payload.isDeleted = false;
  payload.icon = `nav-icon  ${payload.icon}`;
  payload.link = `/${payload.link}`;
  payload.created_at = new Date(`UTC+7`);
  payload.updated_at = new Date(`UTC+7`);
  const menu = await new Menu(payload).save();
  return { data: menu, status: "Data has been created successfully" };
}

async function updateMenu(payload) {
  const validate = validateUpdateSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }
  const filter = { _id: ObjectId(payload._id) };
  let update = payload;

  if (helper.hasKey(payload, "link")) {
    update.link = `/${payload.link}`;
  }
  update.update_at = new Date(`UTC+7`);

  let menu = await Menu.findOneAndUpdate(filter, update, {
    useFindAndModify: false,
    new: true,
  });

  if (helper.isEmptyObject(menu)) {
    return error.errorReturn({ message: "Menu not found" });
  }

  return { data: menu, status: "Data has been updated successfully" };
}
async function deleteMenu(payload) {
  const validate = validateIdSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }
  const filter = { _id: payload._id };
  const update = {
    updated_at: new Date(`UTC+7`),
    isDeleted: true,
  };
  let menu = await Menu.findOneAndUpdate(filter, update, {
    useFindAndModify: false,
    new: true,
  });
  if (helper.isEmptyObject(menu)) {
    return error.errorReturn({ message: "Menu not found" });
  }

  return { data: menu, status: "Data has been deleted successfully" };
}
async function readAllMenu(query) {
  //query string
  let condition = {
    isDeleted: false,
  };

  //sort
  const sortOrder =
    query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
  const sortField = query.sortBy ? query.sortBy : "date";
  const sort = query.sortBy
    ? {
        [sortField]: sortOrder,
      }
    : {
        name: -1,
      };
  //skip
  const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
  const page = query.page ? Number(query.page) : 1;
  const skip = sizePerPage * page - sizePerPage;
  //limit
  const limit = sizePerPage;
  const menu = await Menu.find(condition)
    .sort(sort)
    .skip(skip)
    .limit(limit)
    .select("-__v -created_at -updated_at -isDeleted");

  const menuAll = await Menu.find(condition);
  if (helper.isEmptyObject(menu)) {
    return {
      foundData: [],
      currentPage: page,
      countPages: 0,
      countData: 0,
    };
  }
  let result = [];

  menu.forEach(async (_menu) => {
    let _menu_result = {
      name: _menu.name,
      icon: _menu.icon.replace("nav-icon ", ""),
      link: _menu.link.replace("/", ""),
      _id: _menu._id,
    };
    result.push(_menu_result);
  });

  return {
    foundData: result,
    currentPage: page,
    countPages: Math.ceil(Object.keys(menuAll).length / sizePerPage),
    countData: Object.keys(menuAll).length,
  };
}
async function readByIdMenu(payload) {
  const validate = validateIdSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }
  const filter = { _id: payload._id };

  let menu = await Menu.findOne(filter);

  if (helper.isEmptyObject(menu)) {
    return error.errorReturn({ message: "Menu not found" });
  }

  return menu;
}
async function readAllMenuSubmenu() {
  let menu = await Menu.find({ isDeleted: false }).cursor();
  let result = [];

  for (
    let _menu = await menu.next();
    _menu != null;
    _menu = await menu.next()
  ) {
    let _menu_result = {
      name: _menu.name,
      icon: _menu.icon,
      link: _menu.link,
    };
    let filter = { menu: _menu._id };
    let submenu = await Submenu.find(filter).cursor();
    let details = [];
    for (
      let _submenu = await submenu.next();
      _submenu != null;
      _submenu = await submenu.next()
    ) {
      let _submenu_result = {
        name: _submenu.name,
        link: _menu.link + _submenu.link,
      };
      details.push(_submenu_result);
    }
    _menu_result.details = details;
    result.push(_menu_result);
  }

  if (helper.isEmptyObject(result)) {
    return error.errorReturn({ message: "Menu not found" });
  }

  return result;
}

module.exports = {
  createMenu,
  updateMenu,
  deleteMenu,
  readByIdMenu,
  readAllMenu,
  readAllMenuSubmenu,
};
