require("dotenv").config();
const debug = require("debug")("backend-int:user-modules");
const jwt = require("jsonwebtoken");
const axios = require("axios");
const error = require("../common/errorMessage");
const helper = require("../common/helper");
const jwt_decode = require("jwt-decode");

async function getAllDepartment(query) {
  try {
    debug(query);
    debug("==> QUERY GET ALL DEPARTEMENT");

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );

    const new_body = {};
    const new_query = {};
    const new_url = "".concat(
      process.env.GATEWAY_IP,
      ":",
      process.env.GATEWAY_PORT,
      "/api/user/department"
    );

    const result = await axios.get(new_url, {
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `${query.user_token}`,
      },
      params: new_query,
    });

    if (result?.error) {
      debug(result);
      debug("===> RESULT GET ALL DEPARTEMENT");
      return result;
    }

    debug(result.data);
    debug("===> RESULT GET ALL DEPARTEMENT");

    return result.data;
  } catch (e) {
    debug(e);
    debug("==> ERROR GET ALL DEPARTEMENT");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function getAllRole(query) {
  try {
    debug(query);
    debug("==> QUERY GET ALL ROLE");

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );

    const new_body = {};
    const new_query = {};
    const new_url = "".concat(
      process.env.GATEWAY_IP,
      ":",
      process.env.GATEWAY_PORT,
      "/api/user/role/"
    );

    const result = await axios.post(new_url, new_body, {
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `${query.user_token}`,
      },
      params: new_query,
    });

    if (result?.error) {
      debug(result);
      debug("===> RESULT GET ALL ROLE");
      return result;
    }

    debug(result.data);
    debug("===> RESULT GET ALL ROLE");

    return result.data;
  } catch (e) {
    debug(e);
    debug("==> ERROR GET ALL ROLE");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function getUserBy(query) {
  try {
    debug(query);
    debug("==> QUERY GET USER BY");

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );

    const new_body = {};
    const new_query = {};
    const new_url = "".concat(
      process.env.GATEWAY_IP,
      ":",
      process.env.GATEWAY_PORT,
      "/api/user/get-user-by-dept-div-pos"
    );

    if (query?.dept) {
      new_query.dept = query.dept;
    }
    if (query?.div) {
      new_query.div = query.div;
    }
    if (query?.pos) {
      new_query.pos = query.pos;
    }

    if (helper.isEmptyObject(new_query)) {
      return error.errorReturn({ message: "dept or div or pos is required" });
    }

    const result = await axios.post(new_url, new_body, {
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `${query.user_token}`,
      },
      params: new_query,
    });

    if (result?.error) {
      debug(result);
      debug("===> RESULT GET USER BY");
      return result;
    }

    debug(result.data);
    debug("===> RESULT GET USER BY");

    return result.data;
  } catch (e) {
    debug(e);
    debug("==> ERROR GET USER BY");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function getUserById(query) {
  try {
    debug(query);
    debug("==> QUERY GET USER BY ID");

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );

    const new_body = {};
    const new_query = {};
    const new_url = "".concat(
      process.env.GATEWAY_IP,
      ":",
      process.env.GATEWAY_PORT,
      "/api/user/get-detail"
    );

    if (query.id) {
      new_query.nik = query.id;
    }

    if (helper.isEmptyObject(query.id)) {
      return error.errorReturn({ message: "id is required" });
    }

    const result = await axios.get(new_url, {
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `${query.user_token}`,
      },
      params: new_query,
    });

    if (result?.error) {
      debug(result);
      debug("===> RESULT GET USER BY ID");
      return result;
    }

    debug(result.data);
    debug("===> RESULT GET USER BY ID");

    return result.data;
  } catch (e) {
    debug(e);
    debug("==> ERROR GET USER BY ID");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function getUpperLevel(query) {
  try {
    debug(query);
    debug("==> QUERY GET USER UPPER LEVEL");

    const validate = Joi.object({
      level: Joi.string().required().valid("one", "two"),
      username: Joi.string(),
      user_token: Joi.string(),
    }).validate(query);

    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );

    const new_body = {};
    const new_query = {
      level: query.level,
      username: query?.username ?? jwt_decode(query.user_token).username,
    };
    const new_url = "".concat(
      process.env.GATEWAY_IP,
      ":",
      process.env.GATEWAY_PORT,
      "/api/user/upperlevel"
    );

    debug(new_query);

    const result = await axios.get(new_url, {
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `${query.user_token}`,
      },
      params: new_query,
    });

    if (result?.error) {
      debug(result);
      debug("===> RESULT GET USER UPPER LEVEL");
      return result;
    }

    debug(result.data);
    debug("===> RESULT GET USER UPPER LEVEL");

    return result.data;
  } catch (e) {
    debug(e);
    debug("==> ERROR GET USER UPPER LEVEL");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

async function getUnderLevel(query) {
  try {
    debug(query);
    debug("==> QUERY GET USER UNDER LEVEL");

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );

    const new_body = {};
    const new_query = {};
    const new_url = "".concat(
      process.env.GATEWAY_IP,
      ":",
      process.env.GATEWAY_PORT,
      "/api/user/get-under-level"
    );

    if (query.id) {
      new_query.nik = query.id;
    }

    if (helper.isEmptyObject(query.id)) {
      return error.errorReturn({ message: "id is required" });
    }

    const result = await axios.get(new_url, {
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `${query.user_token}`,
      },
      params: new_query,
    });

    if (result?.error) {
      debug(result);
      debug("===> RESULT GET USER UNDER LEVEL");
      return result;
    }

    debug(result.data);
    debug("===> RESULT GET USER UNDER LEVEL");

    return result.data;
  } catch (e) {
    debug(e);
    debug("==> ERROR GET USER UNDER LEVEL");
    return error.errorReturn({ message: "Internal Server Error" });
  }
}

module.exports = {
  getAllDepartment,
  getAllRole,
  getUserBy,
  getUserById,
  getUpperLevel,
  getUnderLevel,
};
