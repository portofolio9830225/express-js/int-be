const moment = require("moment");
const { readFileSync, unlinkSync } = require("fs");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const dateFormat = require("dateformat");


async function sendEmail(subject, payload, send_to, send_cc) {
    const date_now = new Date();
    const jwt_token = jwt.sign(
    {
      name: process.env.NAME,
      type: "private_apps",
    },
    process.env.PRIVATE_KEY,
    { expiresIn: "2400h" }
  );

  const html = JSON.stringify(payload);
  const body = {
    name: "Coin",
   "send_to": send_to,
   "send_cc": send_cc,
    category: "mail",
    body_type: "html",
    subject: `Int Transaction - ${subject}`,
    body: html,
    sending_type: "now",
    sending_time: dateFormat(date_now, "yyyy-mm-dd HH:ss"),
    sending_day: "",
    sending_date: "",
    periode_start: dateFormat(date_now, "yyyy-mm-dd HH:ss"),
    periode_end: dateFormat(date_now, "yyyy-mm-dd HH:ss"),
  };
  const parameters = {
    method: "POST",
    url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/alert/`,
    headers: {
      "X-Public-Key": process.env.PUBLIC_KEY,
      "X-Application-Token": `Bearer ${jwt_token}`,
    },
    data: body,
  };

  const result = await axios(parameters);
  return {result: result}
}

module.exports = {
  sendEmail
};
