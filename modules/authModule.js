const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-int:auth-module");
const helper = require("../common/helper");
const db = require("../models");
const Authentication = db.authentication;
const jwt = require("jsonwebtoken");
const Encryptor = require("../modules/Encryptor");
const axios = require("axios");

const createSchema = Joi.object({
  username: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}

async function login(params) {
  try {
    //check username on mongodb
    const filterAuth = {
      is_active: true,
      username: params.username,
    };
    debug("=====");
    const authen = await Authentication.findOne(filterAuth);

    debug(authen + " hasil");
    if (helper.isEmptyObject(authen)) {
      return error.errorReturn({
        message: "Username is not registered yet. Please contact Admin!",
      });
    }

    const jwt_token = jwt.sign(
      {
        name: process.env.NAME,
        type: "private_apps",
      },
      process.env.PRIVATE_KEY,
      { expiresIn: String(process.env.SESSION_TIME) }
    );
    const body = {
      username: params.username,
      password: params.password,
      auth_type: "user",
    };
    const parameters = {
      method: "GET",
      url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/user`,
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
      },
      data: body,
    };

    const result = await axios(parameters);
    return result;
  } catch (err) {
    debug(err.response);
    return error.errorReturn({ message: err.response.data.message });
  }
}

async function register(payload) {
  try {
    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    //check exists
    const filterAuth = {
      username: payload.username,
    };
    const check_data = await Authentication.findOne(filterAuth);
    if (check_data) {
      return error.errorReturn({ message: "Data has already exists!" });
    }
    payload.is_active = true;
    const auth = await new Authentication(payload).save();
    return auth;
  } catch (err) {
    debug(err.response);
    return error.errorReturn({ message: "Internal server error" });
  }
}
async function createAppToken(payload) {
  try {
    debug(payload);
    const jwt_token = jwt.sign(
      {
        name: payload.name,
        type: "private_apps",
      },
      process.env.PRIVATE_KEY,
      { expiresIn: String(process.env.SESSION_TIME) }
    );
    return { appToken: jwt_token };
  } catch (err) {
    debug(err.response);
    return error.errorReturn({ message: "Internal server error" });
  }
}
module.exports = {
  login,
  register,
  createAppToken,
};
