const debug = require("debug")("backend-int:backendvalidator-middleware");
const jwt = require("jsonwebtoken");
const util = require("util");
const jwtVerifyAsync = util.promisify(jwt.verify);
const error = require("../common/errorMessage");
const db = require("../models");
const Menu = db.menu;
const Submenu = db.submenu;
const Authorization = db.authorization;
const SetActiveLog = db.set_active_log;
const helper = require("../common/helper");
const logModule = require("../modules/logModule");

isValidApp = async (req, res, next) => {
  try {
    const appName = req.header("x-app-name");
    const publicKey = req.header("x-public-key");

    if (!appName)
      return res
        .status(401)
        .send(error.errorReturn({ message: "Access Denied" }));
    if (appName != process.env.NAME)
      return res
        .status(401)
        .send(error.errorReturn({ message: "Access Denied" }));
    if (publicKey != process.env.PUBLIC_KEY)
      return res
        .status(401)
        .send(error.errorReturn({ message: "Access Denied" }));
    next();
  } catch (err) {
    debug(err);
    return res
      .status(500)
      .send(error.errorReturn({ message: "Internal Server Error" }));
  }
};
isValidRequest = async (req, res, next) => {
  try {
    const x_token = req.header("X-Application-Token");

    if (!x_token)
      return res
        .status(401)
        .send(error.errorReturn({ message: "Access Denied" }));
    const split_token = x_token.split(" ");
    if (split_token.length < 2)
      return res
        .status(401)
        .send(error.errorReturn({ message: "Access Denied" }));
    //verify APP_TOKEN
    const token = split_token[1];
    const verify_token = jwt.verify(token, process.env.PRIVATE_KEY);
    //verify payload APP_TOKEN
    if (verify_token.name != process.env.NAME)
      return res
        .status(401)
        .send(error.errorReturn({ message: "Access Denied" }));

    let url = req.originalUrl.replace("/api/", "");
    const x_user_token = req.header("x-user-token");
    debug(x_user_token + " ==> x_user_token");
    debug(x_token + " ==> x_token");
    if (!url.includes("auth")) {
      const x_user_token = req.header("x-user-token");

      if (!x_user_token)
        return res
          .status(401)
          .send(error.errorReturn({ message: "Please login first" }));
      const split_user_token = x_user_token.split(" ");
      if (split_user_token.length < 2)
        return res
          .status(401)
          .send(error.errorReturn({ message: "Please login first" }));
      const user_token = split_user_token[1];
      //verify USER_TOKEN
      const checkUserToken = await jwtVerifyAsync(
        user_token,
        process.env.PRIVATE_KEY
      );
      if (!checkUserToken)
        return res
          .status(401)
          .send(error.errorReturn({ message: "Access Denied" }));
      //send log
      await send_log(req, url, checkUserToken);
    }
    if (
      req.header("x-user-token") &&
      (url.includes("crud") || url.includes("notification"))
    ) {
      req.body.user_token = x_user_token;
    }
    return next();
  } catch (err) {
    debug("error =>", err.message);
    if (err.message == "jwt expired")
      return res.status(401).send(
        error.errorReturn({
          message: "Session is expired. Please login first",
        })
      );
    return res
      .status(500)
      .send(error.errorReturn({ message: "Internal Server Error" }));
  }
};

checkDuplicateNameLink = (req, res, next) => {
  Menu.findOne({
    name: req.body.name,
  }).exec((err, menu) => {
    if (err) {
      res.status(500).send({ error: true, message: err });
      return;
    }

    if (menu) {
      res
        .status(400)
        .send({ error: true, message: "Failed! Name is already in use!" });
      return;
    }

    Menu.findOne({
      link: `/${req.body.link}`,
    }).exec((err, menu) => {
      if (err) {
        res.status(500).send({ error: true, message: err });
        return;
      }

      if (menu) {
        res
          .status(400)
          .send({ error: true, message: "Failed! Link is already in use!" });
        return;
      }

      next();
    });
  });
};

checkDuplicateNameLinkSubmenu = (req, res, next) => {
  Submenu.findOne({
    name: req.body.name,
  }).exec((err, menu) => {
    if (err) {
      res.status(500).send({ error: true, message: err });
      return;
    }

    if (menu) {
      res
        .status(400)
        .send({ error: true, message: "Failed! Name is already in use!" });
      return;
    }

    Submenu.findOne({
      link: `/${req.body.link}`,
    }).exec((err, menu) => {
      if (err) {
        res.status(500).send({ error: true, message: err });
        return;
      }

      if (menu) {
        res
          .status(400)
          .send({ error: true, message: "Failed! Link is already in use!" });
        return;
      }

      next();
    });
  });
};

async function send_log(req, url, checkUserToken) {
  const checkActiveLog = await SetActiveLog.find({
    is_active: true,
  }).exec();
  if (!helper.isEmptyObject(checkActiveLog)) {
    const ipClient = await logModule.getCallerIP(req);
    //send log
    let payloadLog = {
      method: "POST",
      body: {
        access_url: `${ipClient}/${url}`,
        requester: checkUserToken.username,
        apps: process.env.NAME,
        ip: ipClient,
      },
    };

    return await logModule.sendLog(payloadLog);
  }
  return;
}

checkDuplicateRole = (req, res, next) => {
  if (!req.body.role) {
    res.status(400).send({ error: true, message: "Failed! Role is required!" });
  }
  Authorization.findOne({
    role: req.body.role.toLowerCase(),
  }).exec((err, menu) => {
    if (err) {
      res.status(500).send({ error: true, message: err });
      return;
    }

    if (menu) {
      res.status(400).send({
        error: true,
        message: "Failed! Role is already has authorization!",
      });
      return;
    }

    next();
  });
};

const BackendValidator = {
  isValidRequest,
  checkDuplicateNameLink,
  checkDuplicateNameLinkSubmenu,
  checkDuplicateRole,
  isValidApp,
};

module.exports = BackendValidator;
