require("dotenv").config();
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const helmet = require("helmet");
const debug = require("debug")("backend-int:server");
const error = require("./common/errorMessage");
const rfs = require("rotating-file-stream");
const app = express();
const cors = require("cors");

app.use(cors());

if (process.env.NODE_ENV === "production") {
	const accessLogStream = rfs.createStream("server.log", {
		interval: "1d", // rotate daily
		path: path.join(__dirname, "log"),
	});
	app.use(logger("combined", { stream: accessLogStream }));
} else {
	app.use(logger("combined"));
}
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(bodyParser.json({ limit: "5mb" }));

app.use(express.static(path.join(__dirname, "public")));

// mongodb
// global.int_db = require("./config/database");
const db = require("./models");
let MONGO_URI = process.env.NODE_ENV == "test" ? process.env.MONGO_CLOUD_TEST : process.env.MONGO_CLOUD;
debug(`will connect to : ${MONGO_URI}`);
db.mongoose
	.connect(`${MONGO_URI}`, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		auth: { authSource: "admin" },
		user: `${process.env.MONGO_USERNAME}`,
		pass: `${process.env.MONGO_PASSWORD}`,
	})
	.then(() => {
		debug("Successfully connect to MongoDB.");
	})
	.catch(err => {
		debug("Connection error", err);
		process.exit();
	});

//checking health pod
app.get("/health", (req, res) => {
	res.send(`Apps is healthy`);
});
app.get("/", (req, res) => {
	res.send(`Welcome to Backend INT`);
});

let routes = [
  // default
  { path: "/auth", router: require("./routes/auth") },
  { path: "/menu", router: require("./routes/menu") },
  { path: "/submenu", router: require("./routes/submenu") },
  { path: "/authorization", router: require("./routes/authorization") },
  { path: "/user", router: require("./routes/user") },
  // add new endpoint
  { path: "/customer", router: require("./routes/v2/customer") },
  { path: "/pic", router: require("./routes/v2/pic") },
  { path: "/transaksi", router: require("./routes/v2/transaksi") },
  { path: "/transaction", router: require("./routes/v2/rio/importTransaction") },
  { path: "/product", router: require("./routes/v2/azhar/product") },
];

routes.forEach(({ path, router }) => {
	app.use(`/api${path}`, router);
});

app.use((req, res, next) => {
	return res.status(404).send(error.errorReturn({ message: "Invalid routes!" }));
});
module.exports = app;
