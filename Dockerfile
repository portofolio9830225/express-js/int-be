FROM node:14-buster 
# create dir 
RUN apt-get update -y && apt-get install rsync -y 
WORKDIR /usr/src/app 

# build dependencies 
COPY ./package*.json ./ 
RUN npm install 
 
# Bundle app source 
COPY . . 
EXPOSE 3300
# start express server 

CMD [ "npm", "start" ] 